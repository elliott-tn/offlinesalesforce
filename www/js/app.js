angular.module('starter', ['ionic', 'starter.services', 'starter.controllers', 'ngCordova'])

.run(['$ionicPlatform', '$rootScope', 'NetworkService', 'AppRunStatusService', 'SyncService', 'InitialService', '$ionicLoading', function($ionicPlatform, $rootScope, NetworkService, AppRunStatusService, SyncService, InitialService, $ionicLoading) {

    $rootScope.resourcePath = window.RESOURCE_ROOT;
    $rootScope.appstatus = "not set";

  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleLightContent();
    }

    // Local Notification plugin
    if (cordova && cordova.plugins && cordova.plugins.notification) {
      if(device.platform === "iOS") {
         window.plugin.notification.local.promptForPermission();
      }
      cordova.plugins.notification.local.on("trigger", function (notification, state) {
        NotificationService.handleLocalNotification(notification.id, state);
      });
      cordova.plugins.notification.local.on("click", function (notification, state) {
        NotificationService.handleLocalNotificationClick(notification.id, state);
      });
    }

    document.addEventListener("resume", function() {
      AppRunStatusService.statusEvent('resume');
    }, false);
    document.addEventListener("online", function() {
        NetworkService.networkEvent('online');
    }, false);
    document.addEventListener("offline", function() {
        NetworkService.networkEvent('offline');
    }, false);

    if (screen && screen.lockOrientation) {
      screen.lockOrientation('portrait');
    }

  });

  // handle feedback from syncing of mobile tables
  $rootScope.$on('syncTables', function(event, args) {
      $rootScope.$broadcast('handleSyncTables', args);
  });

 InitialService.hasDoneProcess("initialDataLoaded").then(function(result) {
    if (result) {
      // If we've already installed the app, and done an initial load of data, then sync tables using standard synchronous call
          $ionicLoading.show({
            template: '<p id="app-progress-msg" class="item-icon-left"><h3>Loading Data General Sync...</h3></p>',
            animation: 'fade-in',
            showBackdrop: true,
                duration: 30000
        });
  SyncService.syncTables(['Lead__ap']);
        console.log('%cELLIOTTLOG | initialSync already performed, calling a general syncTables', 'font-weight: bold');
   } else {
      // Initial install and load of data => we can do a faster asynchronous load of tables.
      // Calls devUtils.initialSync (from within SyncService) rather than usual devUtils.syncMobileTable call
       $ionicLoading.show({
            template: '<p id="app-progress-msg" class="item-icon-left"><h3>Loading Data Initial Sync...</h3><ion-spinner></ion-spinner></p>',
            animation: 'fade-in',
            showBackdrop: true,
                duration: 30000
        });
    SyncService.initialSync(['Lead__ap']);
       console.log('%cELLIOTTLOG | initialSync not already performed, calling an initialSync', 'font-weight: bold');
      // Save the fact that we've run the initial data load for the app install
    InitialService.setProcessDone("initialDataLoaded");
   }
 });

}])



    //==    SET UP THE APP STATES   ==//


.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
    $stateProvider
    
    // setup an abstract state for the tabs directive
    
        .state('tab', {
            url: "/tab",
            abstract: true,
            templateUrl: RESOURCE_ROOT +  'templates/tabsMain.html'
        })
    
        .state('tab.home', {
            url: '/home',
            views: {
                'home-tab': {
                    templateUrl: RESOURCE_ROOT +  'templates/home.html',
                    controller: 'HomeControl'
                }
            }
        })
    
        .state('tab.leads', {
            url: '/leads',
            views: {
                'leads-tab': {
                    templateUrl: RESOURCE_ROOT +  'templates/leads.html',
                    controller: 'LeadsControl'
                }
            }
        })
    
        .state('tab.leads-detail', {
            url: '/leads/:leadId',
            views: {
                'leads-tab': {
                    templateUrl: RESOURCE_ROOT +  'templates/leaddetail.html',
                    controller: 'LeadDetailControl'
                }
            }
        })
    
        .state('tab.leads-newlead', {
            url: '/leads/:leadId/newlead',
            views: {
                'leads-tab': {
                    templateUrl: RESOURCE_ROOT +  'templates/newlead.html',
                    controller: 'NewLeadControl'
                }
            }
        })
    
    //==    MC DEV SETTINGS' TABS   ==//
    
    
        .state('tab.settings', {
            url: '/settings',
            views: {
                'settings-tab': {
                    templateUrl: RESOURCE_ROOT +  'templates/settings.html',
                    controller: 'SettingsCtrl'
                }
            }
        })

        .state('tab.settings-devtools', {
            url: '/settings/devtools',
            views: {
                'settings-tab': {
                    templateUrl: RESOURCE_ROOT +  'templates/settingsDevTools.html',
                    controller: 'SettingsCtrl'
                }
            }
        })

        .state('tab.settings-mti', {
            url: '/settings/mti',
            views: {
                'settings-tab': {
                    templateUrl: RESOURCE_ROOT +  'templates/settingsDevMTI.html',
                    controller: 'MTICtrl'
                }
            }
        })

        .state('tab.mti-detail', {
            url: '/settings/mti/:tableName',
            views: {
                'settings-tab': {
                    templateUrl: RESOURCE_ROOT +  'templates/settingsDevMTIDetail.html',
                    controller: 'MTIDetailCtrl'
                }
            }
        })

        .state('tab.settings-testing', {
            url: '/settings/testing',
            views: {
                'settings-tab': {
                    templateUrl: RESOURCE_ROOT +  'templates/settingsTesting.html',
                    controller: 'TestingCtrl'
                }
            }
        })

        .state('tab.settings-deploy', {
            url: '/settings/deploy',
            views: {
                'settings-tab': {
                    templateUrl: RESOURCE_ROOT +  'templates/settingsDeploy.html',
                    controller: 'DeployCtrl'
                }
            }
        });

    
    //==    DEFAULT FIRST STATE ==//
    
    
    $urlRouterProvider.otherwise('/tab/home');

}]);


    //==    CHECK EVERYTHING IS HEALTHY, IF SO BOOT ANGULAR AND THE APP    ==//


function myapp_callback(runUpInfo) {
  if (typeof(runUpInfo) != "undefined" &&
     (typeof(runUpInfo.newVsn) != "undefined" && runUpInfo.newVsn != runUpInfo.curVsn)) {
      
    // going to call a hardReset as an upgrade is available.
      
    console.debug('runUpInfo', runUpInfo);
    var vsnUtils= mobileCaddy.require('mobileCaddy/vsnUtils');
    vsnUtils.hardReset();
  } else {
      
    // everything is okay, boot angular and start up the client app.
      
    angular.bootstrap(document, ['starter']);
  }
}